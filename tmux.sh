#!/bin/sh

# ctrl+b d - to detach and exit

INTERVAL="1"
REDIS_PASS="pass"
REDIS_CMD="info replication"

tmux new-session -s "redis-test" \; \
    send-keys "docker-compose logs -f app" C-m \; \
    split-window -h \; \
    send-keys "watch --interval $INTERVAL docker-compose exec redis-node-1 redis-cli -a pass $REDIS_CMD" C-m \; \
    select-pane -t 0 \; \
    split-window -h \; \
    send-keys "watch --interval $INTERVAL docker-compose exec redis-node-0 redis-cli -a pass $REDIS_CMD" C-m \; \
    select-pane -t 2 \; \
    split-window -h \; \
    send-keys "watch --interval $INTERVAL docker-compose exec redis-node-2 redis-cli -a pass $REDIS_CMD" C-m \; \
    select-pane -t 1 \; \
    split-window -v \; \
    send-keys "watch --interval $INTERVAL docker-compose exec redis-node-3 redis-cli -a pass $REDIS_CMD" C-m \; \
    select-pane -t 3 \; \
    split-window -v \; \
    send-keys "watch --interval $INTERVAL docker-compose exec redis-node-4 redis-cli -a pass $REDIS_CMD" C-m \; \
    select-pane -t 5 \; \
    split-window -v \; \
    send-keys "watch --interval $INTERVAL docker-compose exec redis-node-5 redis-cli -a pass $REDIS_CMD" C-m \; \

tmux kill-session -t "redis-test"
