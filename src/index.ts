import { Cluster } from 'ioredis';

let redis: Cluster | undefined;

async function getRedis(): Promise<Cluster> {
  if (redis) {
    return redis;
  }

  const cluster = await(
    new Cluster(
      [
        { host: '192.168.144.100', port: 6379 },
        /*{ host: '192.168.144.101', port: 6379 },
        { host: '192.168.144.102', port: 6379 },
        { host: '192.168.144.103', port: 6379 },
        { host: '192.168.144.104', port: 6379 },
        { host: '192.168.144.105', port: 6379 },*/
      ],
    {
      redisOptions: {
        password: 'pass',
      },
    })
  );

  redis = cluster;
  return redis;
}

function randomString(len: number): string {
  return (Math.random() + 1).toString(36).substring(2, len);
}

async function getKeys(pattern = '*', redis: Cluster | undefined = undefined): Promise<string[]> {
  if (!redis) {
    redis = await getRedis();
  }

  let keys: string[] = [];
  const nodes = redis.nodes('master');

  await Promise.all(nodes.map(async (node) => {
    let nodeKeys = await node.keys(pattern);
    keys = [...keys, ...nodeKeys];
  }));

  return [...new Set(keys)];
}

function log(message: string): void {
  console.log(`[${Date.now()}] ${message}`);
}

async function worker() {
  try {
    const redis = await getRedis();
    const key = randomString(6);
    await redis.set(key, randomString(10));
    await redis.expire(key, 120);
    const keys = await getKeys('*');
    const value = await redis.get(key);
    
    log(`${keys.includes(key) ? 1 : 0}/${keys.length} - ${key}: ${value}`);
  } catch (e) {
    log('Error');
  }
}

(async () => {
  while(true) {
    worker();
    await new Promise((resolve) => setTimeout(resolve, 200));
  }
})();
