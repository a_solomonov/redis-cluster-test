### Install & Run
```bash
npm install
docker-compose up -d
```

### Attach to containers
```bash
./tmux.sh
```
